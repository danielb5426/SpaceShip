package ship;

import commons.GameGUI;

public class Special extends SpaceShip{

	private SpaceShip _closest;
	private int _turn;
	
	public Special(String type)
	{
		super(type);
		_turn = 1;
	}
	
	public void doAction(SpaceWars game)
    {
		
    	_shield = false;
		_closest = game.getClosestShipTo(this);
		
		_image = GameGUI.SPACE_SHIP;
		
		if (_physics.angleTo(_closest.getPhysics()) > 0)
		{
			_turn = -1;
		}
		else
		{
			_turn = 1;
		}
		
		if (Math.abs(_physics.getAngle() - _closest.getPhysics().getAngle()) <= 0.25)
		{
			_turn = 1;
		}
		
		if ((_closest._type.equals("a") && Math.abs(_physics.getAngle() - _closest.getPhysics().getAngle()) <= 0.10) || (_closest._type.equals("b") && _physics.distanceFrom(_closest.getPhysics()) <= 5));   
		{
			teleport();
		}
		
		_physics.move(true, _turn);
		
		
		
    	
		if(_currentEnergy < _maxEnergy)
    	{
    		_currentEnergy++;
    	}
    }
}
