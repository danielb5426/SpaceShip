package ship;

import commons.GameGUI;

public class Basher extends SpaceShip{

	private SpaceShip _closest;
	public Basher(String args)
	{
		super(args);
	}
	
	public void doAction(SpaceWars game)
    {
    	_shield = false;
		_closest = game.getClosestShipTo(this);
		
		_physics.move(true, 0);
		
		if (_closest._physics.distanceFrom(_closest._physics) <= 0.19)
		{
			shieldOn();
		}
		
		if(_currentEnergy < _maxEnergy)
    	{
    		_currentEnergy++;
    	}
    }

}
