package ship;
import java.lang.Math;

import commons.GameGUI;
import commons.SpaceShipPhysics;


public class HumanShip extends SpaceShip
{
	
	
	public HumanShip(String args)
	{
		super(args);
		
	}
	
	public void doAction(SpaceWars game)
    {
		
		_image = GameGUI.SPACESHIP_IMAGE;
		
		
		if (game.getGUI().isShotPressed()) {
			fire(game);
		}
		
		if (game.getGUI().isTeleportPressed()){
			teleport();
		}
		
		if (game.getGUI().isShieldsPressed()){
			shieldOn();
			_image = GameGUI.SPACESHIP_IMAGE_SHIELD;
		}
		
		if (game.getGUI().isUpPressed()) {
			_physics.move(true, 0);
		}
		
		if (game.getGUI().isLeftPressed()) {
			_physics.move(false, 1);
		}
		
		if (game.getGUI().isRightPressed()) {
			_physics.move(false, -1);
		}
		
    	
    	if(_currentEnergy < _maxEnergy)
    	{
    		_currentEnergy++;
    	}
    	
    }

}
