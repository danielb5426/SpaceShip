package ship;

import commons.GameGUI;

public class Aggressive extends SpaceShip{

	private SpaceShip _closest;
	private int _turn;
	
	public Aggressive(String args) {
		super(args);
	}

	public void doAction(SpaceWars game)
    {
		
    	_shield = false;
		_closest = game.getClosestShipTo(this);
		_turn = 0;
		double angleTo = _physics.angleTo(_closest.getPhysics());
		
		if (angleTo > 0)
		{
			_turn = 1;
		}
		else if (angleTo < 0)
		{
			_turn = -1;
		}
		
		_physics.move(true, _turn);
		
		if (_physics.distanceFrom(_closest._physics) <= 0.19)
		{
			fire(game);
		}
    	
		if(_currentEnergy < _maxEnergy)
    	{
    		_currentEnergy++;
    	}
    	
    	
    }

}
