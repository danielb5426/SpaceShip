package ship;


public class SpaceShipFactory
{
	public static SpaceShip[] createSpaceShips(String[] args)
    {
		SpaceShip ships[] = new SpaceShip[args.length];
		
    	for (int i = 0; i < args.length; i++)
    	{
    		System.out.println(args[i]);
    		if (args[i].equals("r"))
    		{
    			ships[i] = new Runner(args[i]);
    		}
    		else if (args[i].equals("b"))
    		{
    			ships[i] = new Basher(args[i]);
    		}
    		else if (args[i].equals("a"))
    		{
    			ships[i] = new Aggressive(args[i]);
    		}
    		else if (args[i].equals("d"))
    		{
    			ships[i] = new Drankard(args[i]);
    		}
    		else if (args[i].equals("s"))
    		{
    			ships[i] = new Special(args[i]);
    		}
    		else if (args[i].equals("g"))
    		{
    			ships[i] = new Sangoku(args[i]);
    		}
    		else if (args[i].equals("h"))
    		{
    			ships[i] = new HumanShip(args[i]);
    		}
    	}
    	return ships;
    }
}
