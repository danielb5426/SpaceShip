package ship;

import commons.GameGUI;

public class Sangoku extends SpaceShip{

	private SpaceShip _closest;
	private int _turn = 1;
	private int _shooting;
	
	public Sangoku(String args)
	{
		super(args);
		_shooting = 0;
	}
	
	public void fire(SpaceWars game)
    {
       if (_currentEnergy > 0 && _canShot == 7)
       {
    	   _currentEnergy -= 19;
    	    game.addShot(_physics);
    	    _canShot = 0;
    	    _shooting++;
       }
       if (_canShot < 7)
       {
    	   _canShot++;
       }
    }
	
	public void doAction(SpaceWars game)
    {
		
    	_shield = false;
		_closest = game.getClosestShipTo(this);
		_turn = 0;
		
		if (_shooting > 0)
		{
			_image = GameGUI.SANGOKU_KAMEAMEA_IMAGE;
		}
		else
		{
			_image = GameGUI.ENEMY_SPACESHIP_IMAGE;
		}
		
		if (_shooting > 7)
		{
			_shooting = 0;
		}
		
		double angleTo = _physics.angleTo(_closest.getPhysics());
		
		if (angleTo > 0)
		{
			_turn = 1;
		}
		else if (angleTo < 0)
		{
			_turn = -1;
		}
		
		_physics.move(true, _turn);
		
		if (_closest._physics.getAngle() == _physics.getAngle() && _physics.distanceFrom(_closest._physics) < 0.10)
		{
			shieldOn();
		}
		
		if (_physics.distanceFrom(_closest._physics) <= 0.25)
		{
			fire(game);
		}
    	
		if(_currentEnergy < _maxEnergy)
    	{
    		_currentEnergy++;
    	}
    	
    }
}
