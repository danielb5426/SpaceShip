package ship;

import java.awt.Image;
import java.io.IOException;
 
import commons.GameGUI;
import commons.SpaceShipPhysics;

/**
 * The API spaceships need to implement for the SpaceWars game. 
 * It is your decision whether SpaceShip.java will be an interface, an abstract class,
 *  a base class for the other spaceships or any other option you will choose.
 *  
 * @author oop
 */
abstract class SpaceShip
{  

	protected SpaceShipPhysics _physics;
	protected int _maxEnergy;
	protected int _currentEnergy;
	protected int _livePoint;
	protected boolean _shield;
	protected int _canShot;
	protected int _deathCount;
	protected Image _image;
	protected String _type;
	
	
	public SpaceShip(String type)
	{
		_type = type;
		_physics = new SpaceShipPhysics();
		_livePoint = 22;
		_maxEnergy = 220;
		_currentEnergy = 190;
		_shield = false;
		_canShot = 0;
		_deathCount = 0;
		_image = GameGUI.ENEMY_SPACESHIP_IMAGE;
	}
	
    /**
     * Does the actions of this ship for this round. 
     * This is called once per round by the SpaceWars game driver.
     * 
     * @param game the game object to which this ship belongs.
     */
    abstract void doAction(SpaceWars game);

    /**
     * This method is called every time a collision with this ship occurs 
     */
    public void collidedWithAnotherShip()
    {
    	if (_shield == false)
    	{
    		_livePoint--;
    		_maxEnergy -= 10;
    	}
    	else
    	{
    		_maxEnergy += 18;
    		_currentEnergy += 18;
    	}
    }

    /** 
     * This method is called whenever a ship has died. It resets the ship's 
     * attributes, and starts it at a new random position.
     */
    public void reset()
    {
    	_physics = new SpaceShipPhysics();
    	_livePoint = 22;
		_maxEnergy = 220;
		_currentEnergy = 190;
		_canShot = 0;
		_deathCount ++;
    }

    /**
     * Checks if this ship is dead.
     * 
     * @return true if the ship is dead. false otherwise.
     */
    public boolean isDead() 
    {
        return _livePoint == 0;
    }

    /**
     * Gets the physics object that controls this ship.
     * 
     * @return the physics object that controls the ship.
     */
    public SpaceShipPhysics getPhysics()
    {
        return _physics;
    }

    /**
     * This method is called by the SpaceWars game object when ever this ship
     * gets hit by a shot.
     */
    public void gotHit()
    {
    	if (_shield == false)
    	{
    		_livePoint--;
    		_maxEnergy -= 10;
    	}
    }

    /**
     * Gets the image of this ship. This method should return the image of the
     * ship with or without the shield. This will be displayed on the GUI at
     * the end of the round.
     * 
     * @return the image of this ship.
     * @throws IOException 
     */
    public Image getImage()
    {
    	return _image; 
    }

    /**
     * Attempts to fire a shot.
     * 
     * @param game the game object.
     */
    public void fire(SpaceWars game)
    {
       if (_currentEnergy > 0 && _canShot == 7)
       {
    	   _currentEnergy -= 19;
    	    game.addShot(_physics);
    	    _canShot = 0;
       }
       if (_canShot < 7)
       {
    	   _canShot++;
       }
    }

    /**
     * Attempts to turn on the shield.
     */
    public void shieldOn()
    {
    	if (_currentEnergy > 0)
    	{
    		_shield = true;
    		_currentEnergy -= 3;
    		_image = GameGUI.ENEMY_SPACESHIP_IMAGE_SHIELD;
    	}
    }

    /**
     * Attempts to teleport.
     */
    public void teleport()
    {
    	if (_currentEnergy > 0)
    	{
    		_currentEnergy -= 140;
    	    _physics = new SpaceShipPhysics();
    	}
    }
    
}
