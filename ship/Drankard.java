package ship;
import java.util.Random; 
import commons.GameGUI;

public class Drankard extends SpaceShip
{
	
	private int _sleep;
	private Random _rand;  
	private int _turn;
	private int _howManyTime;
	private int _seconds;
	
	public Drankard(String args)
	{
		super(args);
		_sleep = 0;
		_seconds = 0;
		_rand = new Random();
		_howManyTime = _rand.nextInt(500);
		_turn = _rand.nextInt(3);;
	}
	
	public void doAction(SpaceWars game)
    {
		
		if (_seconds == _howManyTime)
		{
			_howManyTime = _rand.nextInt(500);
			_turn = _rand.nextInt(3);
			_seconds = 0;
		}
		
        if (_sleep > 100)
		{
        	_image = GameGUI.ENEMY_SPACESHIP_IMAGE;
			if (_turn == 0)
			{
				_physics.move(true, 0);
			}
			else if (_turn == 1)
			{
				_physics.move(true, 1);
			}
			else
			{
				_physics.move(true, -1);
			}
		}
        else
        {
        	_image = GameGUI.DRUNK_SLEEP_IMAGE;
        }
		
		if (_sleep == 300)
		{
			_sleep = 0;
		}
		
    	
    	if(_currentEnergy < _maxEnergy)
    	{
    		_currentEnergy++;
    	}
    
    	_sleep++;
    	_seconds++;
    }

}
