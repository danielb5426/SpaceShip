package ship;
import java.lang.Math;

public class Runner extends SpaceShip{

	private SpaceShip _closest;
	
	public Runner(String args)
	{
		super(args);
	}
	
	public void doAction(SpaceWars game)
    {
    	
		_closest = game.getClosestShipTo(this);
		
		if (_closest._physics.distanceFrom(_closest._physics) <= 0.25 && Math.abs(_physics.getAngle() - _closest.getPhysics().getAngle()) <= 0.23)
		{
			teleport();
		}
		
    	_physics.move(true, 0);
    	
    	
    	if(_currentEnergy < _maxEnergy)
    	{
    		_currentEnergy++;
    	}
    }

}
